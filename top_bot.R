top <- read_mf_real("data/HPM.dis.Top.real",flipdim=T)
bot1 <- read_mf_real("data/HPM.dis.BOTM1.real",flipdim=T)
bot2 <- read_mf_real("data/HPM.dis.BOTM2.real",flipdim=T)
load("active_R_na.Rdata")
proj <- "+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
"1891460.0, 100.0, 0.0, 5624600.0, 0.0, -100.0"
nrow <- 302
ncol <- 501
res <- 100

xmin <-  1891460.0
xmax <- xmin + ncol*res
ymax  <-  5624600.0
ymin <- ymax  - nrow*res

top1_R <- raster(top,
                 xmn = xmin,
                 xmx = xmax,
                 ymn = ymin,
                 ymx = ymax,
                 crs=proj)*active_R_na
plot(top1_R)


bot1_R <- raster(bot1,
                 xmn = xmin,
                 xmx = xmax,
                 ymn = ymin,
                 ymx = ymax,
                 crs=proj)*active_R_na
plot(bot1_R)

bot2_R <- raster(bot2,
                 xmn = xmin,
                 xmx = xmax,
                 ymn = ymin,
                 ymx = ymax,
                 crs=proj)*active_R_na
plot(bot2_R)

library(sf)
library(sp)
library(raster)

bores <- read.csv("bores.csv") %>% 
  mutate(bore = as.character(BoreNo))


bores_sf <- st_as_sf(bores,coords = c("NZTM_Easting","NZTM_Northing"),remove = F,crs=2193)



bores_sp <- bores_sf
bores_sp <- as(bores_sp, 'Spatial')



top <- extract(bot1_R,bores_sp) 
bot1 <- extract(bot1_R,bores_sp) 
bot2 <- extract(bot2_R,bores_sp) 
bores_sf1 <- bores_sf %>% 
  mutate( bot1 = bot1,
          bot2 = bot2)

write_csv(bores_sf1,"bores_sf1_model_layers")
