# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 09:08:19 2018

@author: pawel.rakowski
"""

import os
import numpy as np
import pandas as pd
import flopy
from flopy.utils import util_array
#load existing model
import flopy.utils.binaryfile as bf
model_location = "C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/modflow"


#read head for SP 300 as new starting head
hdobj = bf.HeadFile('C:/PAWEL/MODELLING/HPM/hpm035/hpm035res - Copy/HPM_M3.hds', precision='single')
hdobj.list_records()
rec = hdobj.get_data(kstpkper=(4,299))

model = "HPM_M3_10sc0001.nam"

os.chdir(model_location)

mt = flopy.modflow.Modflow.load(model)

# change starting heads
mt.bas6.strt = rec
mt.bas6.write_file()


#scenarios data for particles
import feather
path = 'my_data.feather'

scen = feather.read_dataframe(path)
scen2 = scen

def fn1(x):
    res= pd.to_numeric(x,downcast = "integer")
    return(res)

def fn2(x):
    res= x-1
    return(res)

scen2 = scen2.apply(fn1)
scen2[["L","C","R"]] = scen2[["L","C","R"]].apply(fn2)

locations_sc = []

i=1
scen = [1,2,3,4]
for rc in scen:

    scen2_sc_i = scen2[scen2["scenario"]==i]
    scen2_sc_i = scen2_sc_i.drop(columns=["scenario","bore"])
    lst = scen2_sc_i.values.tolist()
    locations_sc.append(lst)
    i=i+1

models = ["HPM_M3_10sc0001.nam","HPM_M3_10sc0002.nam","HPM_M3_10sc0003.nam","HPM_M3_10sc0004.nam"]
m_folders = ["sc1","sc2","sc3","sc4"]


#get porosit layers
folder = "C:/PAWEL/MODELLING/HPM/hpm035/NESI archive/uncertainty and mt3d/run/master/master/Flow_model/"
#load interpolated porosity fields
pel1=util_array.Util2d.load_txt([mt.nrow,mt.ncol],folder+'HPM1._pe',np.float32,'(free)')
pel2=util_array.Util2d.load_txt([mt.nrow,mt.ncol],folder+'HPM2._pe',np.float32,'(free)')
    
por=np.zeros((mt.nlay,mt.nrow,mt.ncol))
    
por[0,:,:]=pel1
por[1,:,:]=pel2



#write model files
iii=0
for mi in models:
    model = models[iii]
    mt = flopy.modflow.Modflow.load(model)
    mt.bas6.strt = rec
    mt.model_ws  = m_folders[iii]
    mt.write_input()

#write modpath    
iii=0
for mi in models:
    model = models[iii]
    locations = locations_sc[iii]
    
#    loci = 0
#    for loc in locations:
#        rci=0
#        for rc in loc:
#            locations[loci][rci]=rc-1
#            rci = rci+1
#            
#        loci=loci+1
    
    #convert to format for flopy
    locations2 = []
    loci = 0
    for loc in locations:
        locations2.append(locations[loci]+locations[loci])
        loci=loci+1
    
    locations2=[locations2]
    
    
    #where to place particles on faces
    sd = flopy.modpath.FaceDataType(drape=1,
                                        verticaldivisions1=3,
                                        horizontaldivisions1=3,
                                        verticaldivisions2=3,
                                        horizontaldivisions2=3,
                                        verticaldivisions3=3,
                                        horizontaldivisions3=3,
                                        verticaldivisions4=3,
                                        horizontaldivisions4=3,
                                        rowdivisions5=0, #bottom
                                        columndivisons5=0,
                                        rowdivisions6=3,
                                        columndivisions6=3)
    particles = flopy.modpath.LRCParticleData(subdivisiondata = sd,lrcregions=locations2)
    
    releasedata = [119,[0],30.5] #ReleaseTimeCount, InitialReleaseTime, ReleaseInterval
    
    particles_g = flopy.modpath.ParticleGroupLRCTemplate(particledata=particles,releasedata = releasedata)
    
    
    exe_name="MPath7"
    mp_name = "mp1"
    
    
    mp = flopy.modpath.Modpath7(mp_name, flowmodel=mt,
                                exe_name=exe_name,
                                model_ws = m_folders[iii])
    
    
    
    #mpbas = flopy.modpath.Modpath7Bas(mp, porosity=0.1)
    

    
    mpbas = flopy.modpath.Modpath7Bas(mp, porosity=por)
    
    
    
    mpsim = flopy.modpath.Modpath7Sim(mp, 
                                      simulationtype='combined',
                                      trackingdirection='backward',
                                      weaksinkoption='pass_through',
                                      weaksourceoption='pass_through',
                                      budgetoutputoption='summary',
                                      #budgetcellnumbers=[1049, 1259],
                                      #traceparticledata=[1, 1000],
                                      referencetime=[3650.],
                                      stoptimeoption='extend',
                                      timepointdata=[119,30.5], #number of time point, point interval
                                      #zonedataoption='on', zones=zones,
                                      particlegroups=particles_g)
    
    # write modpath datasets
    mp.write_input()
    iii =iii+1


    
    

#post process    
iii=0
for mi in models:
    model = models[iii]
    locations = locations_sc[iii]    
    
    
    #mp.run_model()
    model_dir = m_folders[iii]
    fpth = model_dir + mp_name + ".mppth" 
    
    p = flopy.utils.PathlineFile(fpth,verbose = True)
    p0 = p.get_alldata(ge = True)
    
    mm = flopy.plot.ModelMap(model=mt,extent = (34000,34300,12100,12450))
    mm.plot_ibound()
    mm.plot_pathline(p0,layer='all', color='red') 
    
    fpth =  model_dir + "/"+ mp_name + ".timeseries"
    ts = flopy.utils.TimeseriesFile(fpth)
    ts0 = ts.get_alldata()
    
    
    #convert to data frame
    i=0
    for i_ob in p0:
        p0_df = pd.DataFrame.from_records(p0[i])
        if i == 0:
            p0_df_comb = p0_df
        else:
            p0_df_comb = pd.concat([p0_df_comb,p0_df])
        i = i + 1